package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/PuerkitoBio/goquery"
)

func main() {
	url := "http://lavka.cz/program/info/110-02-04-2019-jaroslav-dusek-ctyri-dohody"

	resp, err := http.Get(url)
	if err != nil {
		log.Fatalf("Failed to get list page: %s", err)
	}
	defer resp.Body.Close()

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	// Find the review items
	doc.Find("div.info__tickets-item").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		time := s.Find("span").Text()
		fmt.Printf("Time: %s\n", time)
	})

}
